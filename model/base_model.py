import tensorflow as tf
import os


class BaseModel(object):
    def __init__(self, config):
        self.config = config
        self.sess = None
        self.saver = None

    def initialize_session(self):
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        self.saver = tf.train.Saver()

    def save_session(self):
        if not os.path.exists(self.config.dir_model):
            os.makedirs(self.config.dir_model)
        self.saver.save(self.sess, self.config.dir_model)

    def restore_session(self):
        # print("Reloading the lastest trained model...")
        self.saver.restore(self.sess, self.config.dir_model)

    def close_session(self):
        self.sess.close()
