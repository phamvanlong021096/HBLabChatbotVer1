import tensorflow as tf
import numpy as np
from data_utils import bag_of_word
from .base_model import BaseModel


class ChatbotModel(BaseModel):
    def __init__(self, config):
        super().__init__(config)
        self.X = None
        self.y = None
        self.logits = None
        self.loss = None
        self.class_pred = None
        self.train_op = None

    def add_placeholder(self):
        # shape = [batch_size, len_vocab]
        self.X = tf.placeholder(tf.float32, shape=[None, self.config.len_vocab], name="X")

        self.y = tf.placeholder(tf.int32, shape=[None], name='y')

    def add_logits_op(self):
        weights = {
            'w1': tf.Variable(tf.random_normal([self.config.len_vocab, self.config.n_hidden_1])),
            'w2': tf.Variable(tf.random_normal([self.config.n_hidden_1, self.config.n_hidden_2])),
            'out': tf.Variable(tf.random_normal([self.config.n_hidden_2, self.config.num_class]))
        }

        bias = {
            'b1': tf.Variable(tf.random_normal([self.config.n_hidden_1])),
            'b2': tf.Variable(tf.random_normal([self.config.n_hidden_2])),
            'bout': tf.Variable(tf.random_normal([self.config.num_class]))
        }
        layer1 = tf.nn.relu(tf.add(tf.matmul(self.X, weights['w1']), bias['b1']))
        layer2 = tf.nn.relu(tf.add(tf.matmul(layer1, weights['w2']), bias['b2']))
        self.logits = tf.matmul(layer2, weights['out']) + bias['bout']

    def add_predict_op(self):
        preds = tf.nn.softmax(self.logits)
        self.class_pred = tf.cast(tf.argmax(preds, 1), tf.int32)

    def add_loss_op(self):
        losses = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.logits, labels=self.y))
        self.loss = tf.reduce_mean(losses)

    def add_train_op(self):
        optimizer = tf.train.AdamOptimizer(learning_rate=self.config.lr)
        self.train_op = optimizer.minimize(self.loss)

    def build(self):
        self.add_placeholder()
        self.add_logits_op()
        self.add_predict_op()
        self.add_loss_op()
        self.add_train_op()
        self.initialize_session()

    def run_one_epoch(self, train_x, train_y, batch_size):
        nbatch = (len(train_x) - 1) // batch_size + 1
        avg_loss = 0
        for i in range(nbatch):
            start = i * batch_size
            end = min(start + batch_size, len(train_x))
            train_x_batch = train_x[start: end]
            train_y_batch = train_y[start: end]
            _, loss = self.sess.run([self.train_op, self.loss],
                                    feed_dict={self.X: train_x_batch, self.y: train_y_batch})
            avg_loss += loss / nbatch
        return avg_loss

    def train(self, train_x, train_y):
        for epoch in range(self.config.nepoch):
            avg_loss = self.run_one_epoch(train_x, train_y, self.config.batch_size)
            print("Loss after {0} epoch: {1}".format(epoch, avg_loss))
        self.save_session()
        print("Model saved!")
        self.close_session()

    def response(self, sentence):
        sentence = bag_of_word(sentence, self.config.vocab)
        if np.sum(sentence) == 0:
            return -1
        label = self.sess.run(self.class_pred, feed_dict={self.X: [sentence]})
        return self.config.classes[label[0]]
