import json
import nltk
import numpy as np
from nltk.stem.lancaster import LancasterStemmer

stemmer = LancasterStemmer()


def load_data(file_name):
    with open(file_name) as json_data:
        intents = json.load(json_data)

    words = []
    classes = []
    documents = []
    ignore_words = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', ':', ';', '"', "'", '<', ',',
                    '>', '.', '?', '/', '|']
    for intent in intents['intents']:
        for pattern in intent['patterns']:
            w = nltk.word_tokenize(pattern)
            words.extend(w)
            documents.append((w, intent['tag']))
            if intent['tag'] not in classes:
                classes.append(intent['tag'])

    words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
    words = sorted(list(set(words)))

    classes = sorted(list(set(classes)))

    # create our training data
    train_x = []
    train_y = []
    for doc in documents:
        vector = []
        pattern_words = doc[0]
        pattern_words = [stemmer.stem(w.lower()) for w in pattern_words]
        for w in words:
            if w in pattern_words:
                vector.append(1)
            else:
                vector.append(0)
        train_x.append(vector)
        train_y.append(classes.index(doc[1]))

    return train_x, train_y, words, classes


def clean_up_sentence(sentence):
    sentence_words = nltk.word_tokenize(sentence)
    sentence_words = [stemmer.stem(w.lower()) for w in sentence_words]
    return sentence_words


def bag_of_word(sentence, words):
    sentence_words = clean_up_sentence(sentence)
    bag = []
    for w in words:
        if w in sentence_words:
            bag.append(1)
        else:
            bag.append(0)
    return np.array(bag)
