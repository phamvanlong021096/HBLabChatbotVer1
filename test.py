from config import Config
from model.model import ChatbotModel
from data_utils import load_data


def main():
    train_x, train_y, vocab, classes = load_data('intents.json')
    print("Class:", len(classes))
    print("Len vocab:", len(vocab))
    print(vocab)


if __name__ == '__main__':
    main()
