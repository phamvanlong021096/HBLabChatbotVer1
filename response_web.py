from data_utils import load_data
from config import Config
from model.model import ChatbotModel
from flask import request, Flask, render_template, Response
import json
import random

dont_understand = "I'm afraid I don't understand. I'm sorry!"
app = Flask(__name__, static_folder="static")
app.config.update(
    WTF_CSRF_ENABLED=True,
    SECRET_KEY='you-will-never-guess'
)


def get_answer(response_tag):
    for i in intents['intents']:
        if i['tag'] == response_tag:
            return random.choice(i['responses'])


@app.route("/get_answer")
def response():
    sentence = request.args.get('question')
    print('Question: ', sentence)
    response_tag = model.response(sentence)
    ans = ""
    if response_tag == -1:
        ans = dont_understand
    else:
        ans = get_answer(response_tag)
    print("Ans: ", ans)
    return ans


@app.route('/')
def main():
    return render_template('index.html')


if __name__ == '__main__':
    train_x, train_y, len_vocab, num_class = load_data('intents.json')
    config = Config(len_vocab, num_class)
    model = ChatbotModel(config)
    model.build()
    model.restore_session()
    with open('intents.json') as json_data:
        intents = json.load(json_data)
    app.run(debug=True, host='0.0.0.0', use_reloader=False)
