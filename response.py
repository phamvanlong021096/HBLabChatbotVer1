from data_utils import load_data
from config import Config
from model.model import ChatbotModel
import json
import random


def get_answer(intents, response_tag):
    for i in intents['intents']:
        if i['tag'] == response_tag:
            return random.choice(i['responses'])


def interactive_shell(model):
    print(
        "Hello, I’m HBlab Chatbot. I try to be helpful. (But I’m still just a bot. Sorry!) Type something to get started.")

    with open("intents.json") as json_data:
        intents = json.load(json_data)

    while True:
        try:
            # for python 2
            sentence = raw_input("input> ")
        except NameError:
            # for python 3
            sentence = input("input> ")
        words_raw = sentence.strip().split(" ")
        if words_raw == ["exit"]:
            break
        response_tag = model.response(sentence)
        if response_tag == -1:
            print("I'm afraid I don't understand. I'm sorry!")
        else:
            answer = get_answer(intents, response_tag)
            # print("Tag: ", response_tag)
            print(answer)


def main():
    train_x, train_y, len_vocab, num_class = load_data('intents.json')
    config = Config(len_vocab, num_class)
    model = ChatbotModel(config)
    model.build()
    model.restore_session()

    interactive_shell(model)


if __name__ == '__main__':
    main()
