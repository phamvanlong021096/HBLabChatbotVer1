from config import Config
from model.model import ChatbotModel
from data_utils import load_data


def main():
    train_x, train_y, vocab, classes = load_data('intents.json')
    config = Config(vocab, classes)
    model = ChatbotModel(config)
    model.build()
    model.train(train_x, train_y)


if __name__ == '__main__':
    main()
