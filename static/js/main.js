(function () {
    var Message;
    Message = function (arg) {
        this.text = arg.text, this.message_side = arg.message_side;
        this.draw = function (_this) {
            return function () {
                var $message;
                $message = $($('.message_template').clone().html());
                $message.addClass(_this.message_side).find('.text').html(_this.text);
                $('.messages').append($message);
                return setTimeout(function () {
                    return $message.addClass('appeared');
                }, 0);
            };
        }(this);
        return this;
    };
    $(function () {
        var getMessageText, message_side, sendMessage;
        message_side = 'right';
        getMessageText = function () {
            var $message_input;
            $message_input = $('.message_input');
            return $message_input.val();
        };

        sendMessage = function (text, message_side) {
            var $messages, message;
            if (text.trim() === '') {
                return;
            }
            $('.message_input').val('');
            $messages = $('.messages');
            message = new Message({
                text: text,
                message_side: message_side
            });
            message.draw();
            return $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
        };
        $('.send_message').click(function (e) {
            q = getMessageText();
            sendMessage(q, 'right');
            //console.log(q);
            //console.log('OK');
            $.get(url="/get_answer", data={"question": q,}, function(result_data){
                     if (result_data) {
                        sendMessage(result_data, 'left');
                     }
                }
            );

        });
        $('.message_input').keyup(function (e) {
            if (e.which === 13) {
                q = getMessageText();
                sendMessage(q, 'right');
//                console.log(q);
//                console.log('OK');
                $.get(url="/get_answer", data={"question": q,}, function(result_data){
                         if (result_data) {
                            sendMessage(result_data, 'left');
                         }
                    }
                );
            }
        });

       sendMessage('Hello, I’m HBlab Chatbot. I try to be helpful. (But I’m still just a bot. Sorry!) Type something to get started.', 'left');
//        setTimeout(function () {
//            return sendMessage('Hi Sandy! How are you?', 'right');
//        }, 1000);
//        return setTimeout(function () {
//            return sendMessage('I\'m fine, thank you!', 'right');
//        }, 2000);

    });
}.call(this));