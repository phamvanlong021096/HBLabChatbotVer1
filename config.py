class Config(object):
    def __init__(self, vocab, classes):
        self.dir_model = "results/model.weights/"
        self.vocab = vocab
        self.classes = classes
        self.len_vocab = len(vocab)
        self.num_class = len(classes)
        self.batch_size = 8
        self.n_hidden_1 = 10
        self.n_hidden_2 = 8
        self.nepoch = 5000
        self.lr = 0.001
